ansible-rpi-docker
=================

Installe Docker sur Raspberry Pi

Lancement du rôle Ansible testé depuis un poste linux

Pré-requis
------------

  - Ansible 2.6+ (peut fonctionner avec version inférieure ou supérieure, non testé)
  - Raspbian installé sur un ou plusieurs Raspberry Pi
  - SSH activé sur le Raspberry Pi

Variables du rôle
------------------

  * docker_version: version de docker à installer

Exemple de playbook
----------------
    # Déploiement sur un Rapsberry Pi
    - name: "Installation de Docker sur le Raspberry Pi"
      hosts: rpi1
      remote_user: "pi"
      roles:
        - ansible-rpi-docker
      vars:
        docker_version: 18.06.1~ce~3-0~raspbian


Exemple d'inventaire
---------------------

    [rpi1]
    192.168.1.190 servername=rpi1

    [rpi2]
    192.168.1.191 servername=rpi2



Exemple de lancement (via clef ssh, sans mot de passe via une clef ssh)
--------------------------------------------------
    ansible-playbook -b -i inventory playbook.yml
    # -b, --become        joue le rôle ansible en utilisateur avec privilège (root)
    # -i INVENTORY        fichier d'inventaire

Licence
-------

GNU GENERAL PUBLIC LICENSE v3

Information sur l'auteur
-------------------------

Julien LS - contact@jls42.org  
Site Web : https://jls42.org
